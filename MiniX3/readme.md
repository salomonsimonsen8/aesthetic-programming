Link to program: https://salomonsimonsen8.gitlab.io/aesthetic-programming/MiniX3/index.html
<br>
Link to source: https://gitlab.com/salomonsimonsen8/aesthetic-programming/-/blob/main/MiniX3/sketch.js
![Screenshot__66_](/uploads/3fc7d60ccabb7c67b939d9d84a2199c2/Screenshot__66_.png)

![Screenshot__67_](/uploads/08864c90385238f8ff4aff3ba73e0ac8/Screenshot__67_.png)

#Introduction
What I have produced is a very simple throbber. The throbber is an ellipse with different colors. On the screen it says wait a moment.
If you press the mouse the wait a moment text becomes impatient much?
However if you press any key there is a text added that says still loading…
I have done that because I wanted to make a throbber that I think is pleasing to look at. But also a little provocative

##syntaxes
The syntaxt I have used is pretty generic. I have copied the syntax one by one from the book to learn what the different syntaxes do in order to understand them. I did that cause I didn't have an understanding of how the different syntaxes work.
My loop is this
```
 let num = 12;
 translate(width/2, height/2);
 let cir = 360/num*(frameCount%num);
 rotate(radians(cir));
 noStroke();
 fill(0,191,255);
 ellipse(100,10,200,22);
```

As you can see it's the same as the one in the book.
The first line is the one that determines how many ellipses there are in the throbber.
The second line determines where the throbber is on the screen.
The third one makes it possible for it to go round and round. If I were to change the number to 180 it would only go in half circles.
The fourth line makes the ellipses rotate function.
Nostroke makes it there is no outline in the ellipse.
The ellipse makes an oval.
```
if (mouseIsPressed === true) {
fill(25,25,112);
stroke(255);
textSize(80)
text('Impatient much?',520,530);
} else {
fill(135,206,250);
noStroke();
textSize(20);
text('Wait a moment', 735,600)
```
Here I have made an `if statement`. So it's basically determines that if you press the mouse it makes a text that says impatient much rather that its else statement which is a text that says wait a moment.
```
if (keyIsPressed === true) {
fill(25,25,112);
stroke(255);
textSize(80);
text('Still loading...', 590, 540);
}
```
I also made an `if statement` where if you press any key there is a text coming up on the screen.

### Reflection
When you are on the computer and is consumed by whatever you are doing, the time perceived is a lot different from when you’re doing something different. Sometimes the time feels like it goes a lot faster sometimes it’s the opposite. Often times the throbbers are very simple with one or two colors. It’s shape is often one or two shapes as well. In my experience, this makes it not pleasant to look at. We now perceive throbbers as something negative. We now consider the throbber is something bad, even though the purpose is to let us know that the computer is working hard to provide us what we have asked it to do. Whether that be watching a youtube clip or loading a webpage.
<br>
The reason I have made this throbber is that I wanted to make a throbber for me. A thobber I find pleasing to look at. The reason for that is if I want to wait, I want to have pleasing throbbers to look at to be captivated so the time feels like it goes by faster even though it's not. I feel more impatient with the throbbers that companies use. So I wanted to make something that is more entertaining to look at while not changing too much so the user can still know that it’s a throbber. But if you ask me a pleasing looking throbber. 
I also made some text for those who is more impatient than I am. Trying to tease them a bit. The reason is just pettyness from my side. I find it funny and YOLO so why not make it fun. But seriousness aside our devices do a lot for us even though we don’t realize it most of the time, so why not have them make a petty just cause we can, that way they can tease us a bit for being too impatient.
