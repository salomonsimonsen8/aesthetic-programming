function setup() {
  //create a drawing canvas
  createCanvas(windowWidth, windowHeight);
  frameRate(12);

}

function draw() {

  if (mouseIsPressed === true) {
    fill(25, 25, 112);
    stroke(255);
    textSize(80);
    text('Impatient much?', 520, 530);
  } else {
    noStroke();
    textSize(20);
    text('Wait a moment', 735, 600);
  }

  if (keyIsPressed === true) {
    fill(25, 25, 112);
    stroke(255);
    textSize(80);
    text('Still loading...', 590, 540);
  }

  drawElements();
  background(70, 150);
}


//throbber
function drawElements() {
  let num = 12;
  translate(width / 2, height / 2);
  let cir = 360 / num * (frameCount % num);
  rotate(radians(cir));
  noStroke();
  fill(0, 191, 255);
  ellipse(100, 10, 200, 22);

  fill(255, 0, 0);
  ellipse(80, 10, 150, 22);

  fill(255, 20, 147);
  ellipse(60, 10, 100, 22);

  fill(0, 255, 0);
  ellipse(40, 10, 50, 22);

  fill(0, 191, 255);
  ellipse(20, 10, 50, 22);


}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);

}
