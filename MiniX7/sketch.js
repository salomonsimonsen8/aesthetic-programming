let text1 = 255;
let y = 25;
function setup() {
  createCanvas(1275, 605);
  background(255);
  print("Hello World");
  push();
  noStroke();
  fill(10);
  textFont('Helvetica');
  textSize(20);
  text('Colonial mentality', 230, 310)
  pop();

}


function mousePressed() {

  //SMILEY ONE

  if (mousePressed && (mouseButton === LEFT) && mouseX > 20 && mouseX < 210 && mouseY > 20 && mouseY < 210) {
    push();
    stroke(5);
    fill(135, 206, 250);
    arc(115, 210, 160, 120, -PI, 0, CHORD);
    fill(255);
    ellipse(115, 114, 140, 140);
    fill(222, 184, 135);
    ellipse(115, 114, 110, 110);
    fill(20);
    ellipse(95, 104, 12, 16);
    fill(20);
    ellipse(135, 104, 12, 16);
    fill(255);
    ellipse(95, 102, 6, 6);
    ellipse(135, 102, 6, 6);
    ellipse(98, 106.6, 2, 2);
    ellipse(138, 106.6, 2, 2);
    noFill();
    curve(70, 110, 85, 90, 100, 85, 100, 100);
    noFill();
    curve(110, 70, 145, 90, 130, 85, 100, 100);
    curve(130, 95, 120, 125, 110, 125, 100, 100);
    curve(130, 95, 135, 140, 95, 135, 100, 100);

    fill(254, 130, 140);
    stroke(254, 130, 140);
    ellipse(85, 125, 12, 12);
    fill(254, 130, 140);
    stroke(254, 130, 140);
    ellipse(143, 125, 12, 12);
    pop();
  } else {
    rect(20, 20, 190, 190);

  }


  //SMILEY THREE
  if (mouseX > 20 && mouseX < 210 && mouseY > 210 && mouseY < 400) {
    fill(135, 206, 250);
    arc(99, 400, 160, 120, -PI, 0, CHORD);
    stroke(51);
    fill(255);
    ellipse(95, 300, 140, 140);
    fill(222, 184, 135);
    ellipse(95, 300, 110, 110);
    fill(20);
    ellipse(75, 290, 12, 16);
    fill(20);
    ellipse(115, 290, 12, 16);
    fill(255);
    ellipse(115, 287, 6, 6);
    ellipse(75, 287, 6, 6);
    ellipse(77, 292.6, 2, 2);
    ellipse(117, 292.6, 2, 2);
    noFill();
    curve(80, 350, 80, 270, 65, 275, 90, 150);
    noFill();
    curve(120, 350, 110, 270, 125, 275, 90, 150);
    curve(40, 280, 85, 315, 100, 315, 100, 300);
    noFill();
    curve(90, 365, 75, 330, 115, 335, 110, 300);
    fill(0, 128, 0);
    rect(170, 340, 30, 50);
    quad(200, 340, 170, 340, 180, 320, 190, 320);
    rect(180, 300, 10, 20);
    rect(179, 300, 12, 6);

    textSize(6);
    fill(255)
    noStroke();
    text('TUBORG', 172, 350);
  } else {
    rect(20, 210, 190, 190);
  }


}

function draw() {


  textSize(20);
  fill(05, 0, 190);
  textFont('Times New Roman');
  noStroke();
  text('press press inside some squares', 440, 210);

  textSize(20);
  noStroke();
  textFont('Times New Roman');
  fill(255, 68, 68);
  text('hold mouse inside some squares', 460, 270);

  textSize(20);
  noStroke();
  textFont('Times New Roman');
  fill(40, 111, 218);
  text('press up and down key', 440, 330);

  textSize(20);
  noStroke();
  fill(240, 67, 67);
  text('press left and right key', 460, 390);

  textSize(20);
  noStroke();
  textFont('Times New Roman');
  fill(86, 149, 245);
  text('doubleclick', 440, 450);

  //SMILEY 4
  stroke(1);
  fill(0, 0, 139);
  arc(920, 210, 140, 120, -PI, 0, CHORD);
  fill(222, 184, 135);
  ellipse(917, 114, 140, 140);
  fill(51);
  arc(917, 100, 160, 150, -PI, 0, CHORD);
  fill(51);
  ellipse(844, 115, 20, 80);
  fill(179);
  ellipse(850, 115, 10, 60);
  fill(51)
  ellipse(990, 115, 20, 80);
  fill(179);
  ellipse(985, 115, 10, 60);
  fill(179);
  ellipse(917, 84, 160, 50);
  noFill();
  curve(900, 157, 900, 120, 884, 125, 915, 190);
  noFill();
  curve(940, 157, 940, 120, 955, 125, 925, 190);
  curve(923, 126, 914, 135, 924, 135, 950, 70);
  curve(923, 126, 900, 150, 940, 150, 950, 70);
  stroke(255);
  line(1200, 210, 820, 210);


  if (keyCode == UP_ARROW) {
    y = 210;
  }
  else if (keyCode == DOWN_ARROW) {
    y = 20;
  } else {
    y = 20;
  }

  fill(255);
  rect(820, y, 190, 190);
  push();
  noStroke();
  textSize(20);
  fill(10);
  textFont('Helvetica');
  textSize(20);
  text('Reality', 885, 310)
  pop();
  // END OF SMILEY FOUR


  //outline
  stroke(1)
  line(400, 20, 20, 20);
  line(400, 210, 20, 210);
  line(400, 400, 20, 400);
  line(20, 400, 20, 20);
  line(210, 400, 210, 20);
  line(400, 400, 400, 20);

  line(1200, 20, 820, 20);
  line(1200, 210, 820, 210);
  line(1200, 400, 820, 400);
  line(1200, 400, 1200, 20);
  line(820, 400, 820, 20);
  line(1010, 400, 1010, 20);



  //SMILEY TWO
  if ((mouseX > 210) && (mouseX < 400) && (mouseY > 20) && (mouseY < 210)) {
    push();
    //Igloo
    stroke(51);
    fill(176, 224, 230);
    arc(295, 175, 130, 170, -PI, 0, CHORD);
    fill(70, 130, 180);
    arc(295, 175, 50, 90, -PI, 0, CHORD);
    fill(200);
    arc(295, 175, 30, 70, -PI, 0, CHORD);
    fill(70, 130, 180);
    arc(295, 175, 15, 55, -PI, 0, CHORD);
    noFill();
    curve(390, 176, 360, 165, 320, 165, 100, 150);
    curve(300, 176, 270, 165, 231, 165, 205, 150);
    curve(600, 176, 358, 157, 318, 155, 240, 150);
    curve(500, 146, 272, 155, 232, 155, 205, 150);
    curve(600, 146, 276, 145, 234, 145, 150, 150);
    curve(600, 136, 355, 145, 314, 145, 250, 150);
    curve(350, 150, 352, 135, 307, 135, 190, 120);
    curve(350, 150, 283, 135, 238, 135, 190, 120);
    curve(190, 126, 346, 125, 242, 125, 400, 80);
    curve(180, 80, 340, 115, 250, 115, 400, 90);
    curve(200, 80, 330, 104, 258.5, 105, 350, 70);
    curve(280, 80, 272, 95, 318, 95, 450, 61);
    curve(290, 70, 340, 175, 300, 90, 76, 0, 19, 65, 0);
    curve(300, 70, 250, 175, 290, 90, 500, 0, 19, 205, 0);
    curve(290, 90, 295, 130, 295, 90, 300, 0, 19, 205, 0);
    //face
    fill(255);
    ellipse(295, 160, 30, 30);
    fill(222, 184, 135);
    ellipse(295, 160, 20, 20);
    fill(1);
    ellipse(291, 160, 3, 3);
    ellipse(299, 160, 3, 3);
    noStroke();
    fill(254, 130, 140);
    ellipse(299, 164, 2, 2);
    noStroke();
    fill(254, 130, 140);
    ellipse(291, 164, 2, 2);
    pop();
  } else {
    fill(255);
    rect(210, 20, 190, 190);
  }
}




function keyPressed() {
  //SMILEY FIVE
  if (keyCode === LEFT_ARROW) {
    push();
    stroke(5);
    fill(220, 20, 60);
    square(1035, 95, 100);
    fill(50);
    triangle(1025, 95, 1145, 95, 1085, 30);
    fill(155);
    rect(1073, 145, 25, 45);
    rect(1065, 190, 40, 5);
    fill(255, 255, 0);
    square(1045, 115, 25);
    square(1100, 115, 25);
    fill(51)
    ellipse(1077, 170, 3, 3);
    fill(222, 184, 135);
    ellipse(1113, 130, 20, 20);
    fill(51);
    ellipse(1117, 130, 3, 3);
    ellipse(1109, 130, 3, 3);
    line(1111, 135, 1115, 135);
    rect(1110, 47, 10, 20);
    rect(1108, 44, 14, 5);
  } else if (keyCode === RIGHT_ARROW) {
    rect(1010, 20, 190, 190);
    pop();
  }
}

function doubleClicked() {
  if (doubleClicked) {
    push();
    stroke(5);
    fill(51);
    circle(1090, 310, 130);
    fill(0, 0, 139);
    arc(1090, 400, 130, 90, -PI, 0, CHORD);
    fill(222, 184, 135);
    ellipse(1090, 320, 110, 110);
    fill(51);
    ellipse(1110, 315, 12, 16);
    ellipse(1070, 315, 12, 16);
    fill(255);
    ellipse(1110, 313, 6, 6);
    ellipse(1070, 313, 6, 6);
    ellipse(1112, 318, 2, 2);
    ellipse(1072, 318, 2, 2);
    noFill();
    curve(1000, 350, 1060, 300, 1075, 295, 1080, 310);
    curve(1080, 330, 1105, 295, 1120, 300, 1150, 320);
    curve(1090, 400, 1065, 345, 1105, 355, 1100, 300);
    curve(1053, 290, 1084, 335, 1095, 335, 1115, 315);

    fill(0, 128, 0);
    rect(1160, 340, 30, 50);
    quad(1160, 340, 1190, 340, 1180, 320, 1170, 320);
    rect(1170, 300, 10, 20);
    rect(1169, 300, 12, 6);
    fill(51);
    noStroke();
    ellipse(1100, 270, 70, 20);
    ellipse(1125, 280, 40, 15);

    textSize(7);
    fill(255);
    noStroke();
    text('FAXE', 1166, 350);
    text('KONDI', 1164, 357);
    text('EXTRA', 1164, 364);
    pop();
  } else {
    rect(1020, 210, 190, 190);
  }


}

