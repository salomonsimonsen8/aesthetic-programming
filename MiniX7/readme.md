Link to program: https://salomonsimonsen8.gitlab.io/aesthetic-programming/MiniX7/index.html 
<br>
Link to source: https://gitlab.com/salomonsimonsen8/aesthetic-programming/-/blob/main/MiniX7/sketch.js
<br>
![Screenshot__75_](/uploads/02c9a9f423f9123d9882fa153b77297d/Screenshot__75_.png)
# I remade MiniX2
I have after some consideration chosen to revisit the minix feedback that has meant the most to me. The one that I am most proud of. I have chosen MiniX2 because I wanted it to have more interactions the way I intended it in the first place. I also chose it cause in my heart I wish I didn’t encounter those stereotypes as much as I do. It's a topic that I have strong opinions on and I have so much to say but no ways to say it. 
 <br>
This way I can say a little and in a way that may be a little unconventional. I meet many people who don't believe that I encounter stereotypes in this country. My intentions in the original minix I wanted to educate and spark a discussion. I hope the changes I have made makes it more appealing in the interaction department so there is more discussions
 <br>
## The changes I have made
The changes include that you can interact with the individual squares where in the original you would get all the squares revealed. 
The changes are that mouse have to be in one of the squares to see the emoji. In two of the squares, the mouse have to be clicked inside the squares for it to be revealed. In one square the mouse have to be double clicked for it to be revealed. One of the square you have to press right arrow for it to be revealed. In one of the squares you have to press up and down arrows for it to be revealed. 
 ### My code
The most complicated I have done in this minix is when you press up and down then the square moved up and down so a square is concealed and revealed. 
```
  let y = 20
  if (keyCode == UP_ARROW) {
    y = 210;
  }
  else if (keyCode == DOWN_ARROW) {
    y = 20;
  } else {
    y = 20;
  }
```
I made a `variable` to have a base and then used the variable so that the square moves in the way I want it. I want it to move to the down under square Therefore I had to make an `if-statement` where `y` is 210 that way it will move down to the square below. I also made `else-if` so the square moves back up. I made another `else` to have a square that conceals the emoji.
<br>
I have made a lot of `if-statements` in this minix to make more interactions. This include
<br> 
```
if (keyCode === LEFT_ARROW) {
    push();
    stroke(5);
    fill(220, 20, 60);
    square(1035, 95, 100);
    …
  } else if (keyCode === RIGHT_ARROW) {
    rect(1010, 20, 190, 190);
  }

    if ((mouseX > 210) && (mouseX < 400) && (mouseY > 20) && (mouseY < 210)) {
    push();
    stroke(51);
    fill(176, 224, 230);
    arc(295, 175, 130, 170, -PI, 0, CHORD);
    …
  } else {
    fill(255);
    rect(210, 20, 190, 190);
  }

```

With these changes, I hope it will become more provocative.  In making it more provocative, I hope to achieve more discussions, because I believe discussing representation and colonial mentality is the step one to change things for the better. If we don’t have these discussions the thing remain the same. 
 
In addition, to make it more provocative I made the decision to not hide the colonial mentality and reality texts. This way the user has an idea of what this minix is about. But also to reach the user in a way that may make them aware of their colonial mentality cause in my experience that mentality is so common most people don’t even realize they have that mentality. 
 
#### How can I improve the improvement?
In a way I could work more on it is if I could somehow put speech bubbles in each emoji with experienced stereotypes from people. That way it's not just my story but others as well. I could also add sound to it where people tell that time where they experienced stereotypes. 

##### Relations between Aesthetic programming and digital culture. 
Digital culture is all around us and we often don’t realize it. If we have access to digital world we have access to digital culture.  To me the digital culture is the technical world that we can explore, analyze, reflect on, set laws on etc. It’s the way that these digital technologies shape a new way of communication between humans. It somehow affects our experience. Whether it be that we can be in touch with someone that lives 2000 km away the distance don’t feel that large when you have social media. Or you can play a videogame and you feel the time goes faster because you are consumed by the game. 

“We therefore consider programming to be a dynamic cultural practice and phenomenon, a way of thinking and doing in the world, and a means of understanding some of the complex procedures that underwrite and constitute our lived realities, in order to act upon those realities.” (Soon & Cox, 2020, p14) 

This means that we with Aesthetic programming we can learn to be a part of a cultural practice and understand programming in a way that make it critical but also impactful in a way that challenges the power structures that are un place today. It’s also a way to understand the digital culture and understand the morals and ethics of aesthetic programming. The technical digital world is becoming more normal is our future, so to understand it and be critical of it is an important part of what we need to do in our daily life. 
