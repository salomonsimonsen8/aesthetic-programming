Link to the program: https://salomonsimonsen8.gitlab.io/aesthetic-programming/MiniX1/index.html
<br>
Link to source: https://gitlab.com/salomonsimonsen8/aesthetic-programming/-/blob/main/MiniX1/sketch.js
![Screenshot__62_](/uploads/90e91f71a03816ba91e8b50911182d6a/Screenshot__62_.png)

# MiniX 1
I denne uges MiniX har jeg lavet en sketch af et kamera. Det er ret simpelt i udseendet. Jeg fik lavet nogle rektangler og nogle cirkler, så det nogenlunde ligner en kamera. Derudover fik jeg farvet kameraet.
Jeg har brugt 4 koder i min sketch. `fill(x)`, `rect(x,x,x,x)`, `ellipse(x,x,x,x)`, `quad(x,x,x,x,x,x,x,x)`. I starten så brugte jeg quad fordi det et trapez-look, men jeg syntes detså lidt mærkeligt ud, og jeg har beholdt det og tilpasset det som en rektangel, fordi jeg var for doven til, at erstatte det med en rect koden. Disse 4 koder tillod mig, at lave en sketch af kamera. 
Min første kodning har været lidt frustrerende, da jeg ikke har noget erfaringer med kodning. Det sværeste har nok været, at skulle tænke over, hvilke punkter tilhører, hvilke vinkler, og finde ud af tallene for, at kunne placere dem rigtigt. Såvel som det har været frustrerende er jeg også meget lettet over, at jeg i sidste ende kunne komme op med noget, der   så udmærket ud.  Jeg tror ikke helt, jeg har fundet helt ud af, hvordan jeg skal læse eksempler på koden, jeg har bare prøvet, at kunne afkode, hvilke punkter passer bedst med beskrivelsen af eksemplet og prøvet frem. Nogen gange fungerede det, andre gange stak det helt af!
Jeg synes, at programmet er godt, det er forholdsvist nemt, at finde ud af, når, man   er kommet i gang. Omkring programmering for mit vedkommende, hvis der er instruktioner, så kan, man   finde ud af det, men nogen gange kan referencen være svær, at forstå, men hvis man prøver sig frem kan, man i nogen tilfælde forstå referencen bedre.
<br>
##Koden:

```
function setup() {
  // put setup code here
  createCanvas(550, 550);
  background(1000,1000,1000);
}
```
<br>

```
function draw() {
  // put drawing code here
  fill(200);
  rect(90,150,400,250);
  quad(475,150,475,145,435,145,435,150);
  quad(345,150,330,119,249,119,235,150);
  quad(485,135,485,145,425,145,425,135);
  fill(300);
  quad(332,145,326,129,255,129,247,145);
  fill(200);
  quad(105,150,105,145,145,145,145,150);
  fill(51);
  rect(90,165,400,220);
  fill(245);
  rect(430,180,40,20);
  fill(155);
  ellipse(280,275,225,225);
  fill(245);
  ellipse(280,275,175,175);
  ellipse(358,373,15,15);
  ellipse(145,178,10,10);
  rect(100,170,35,15);
}
```
