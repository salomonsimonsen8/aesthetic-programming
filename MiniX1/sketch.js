function setup() {
  // put setup code here
  createCanvas(550, 550);
  background(1000,1000,1000);
 
}


function draw() {
  // put drawing code here
  fill(200);
  rect(90,150,400,250);
  quad(475,150,475,145,435,145,435,150);
  quad(345,150,330,119,249,119,235,150);
  quad(485,135,485,145,425,145,425,135);
  fill(300);
  quad(332,145,326,129,255,129,247,145);
  fill(200);
  quad(105,150,105,145,145,145,145,150);
  fill(51);
  rect(90,165,400,220);
  fill(245);
  rect(430,180,40,20);
  fill(155);
  ellipse(280,275,225,225);
  fill(245);
  ellipse(280,275,175,175);
  ellipse(358,373,15,15);
  ellipse(145,178,10,10);
  rect(100,170,35,15);
}