let capture;
let r, g, b;
let slider;

function setup() {
  createCanvas(windowWidth, windowHeight);
  capture = createCapture(VIDEO);
  capture.hide();
  colorMode(HSB, 255);
  slider = createSlider(0, 255, 127);
  slider.position(740,250);
}

function draw() {
  //Cameraes
  background(0);
  textSize(25);
  text('press on the image', 1020, 270);
  fill(1);
  text('Press key for truth',100,300);
  fill(2);
  noStroke();

  //camera 1
  image(capture, 0, 0, 320, 240);
  filter(INVERT);
  //camera 2
  image(capture,320,0,320,240);
  //camera 3
  image(capture,640,0,320,240);
  strokeWeight(2);
  stroke(slider.value(), 255, 255);
  fill(slider.value(), 255, 255, 127);
  rect(640,0,320,240);
  //camera 4
  image(capture,960,0,320,240);
  strokeWeight(2);
  stroke(r, g, b);
  fill(r, g, b, 127);
  rect(960,0,320,240);

  if (keyIsPressed === true) {
    fill(2);
    noStroke();
    text('You are being monitored right now',200,400);
  }


  
}

  function mousePressed() {
    // Check if mouse is inside the rect
    let d = dist(mouseX, mouseY, 960,0);
    if (d < 260) {
      // Pick new random color values
      r = random(255);
      g = random(255);
      b = random(255);
    }
  
    

}