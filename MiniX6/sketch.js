let bullets = [];
let enemies = [];
let score = 0;
let boat;
function setup() {
  createCanvas(800, 600);
  boat = new Boat();
  for (let i = 0; i < 10; i++) {
    let enemy = {
      x: random(0, width),
      y: random(-800, 0)
    }
    enemies.push(enemy);
  }

}
function draw() {
  background(0, 0, 200);
  rectMode(CENTER);
  fill(157);
  boat.show();

  //update and draw bullets
  for (let bullet of bullets) {
    bullet.y -= 15;
    circle(bullet.x, bullet.y, 10);

  }

  //update and draw enemies
  for (let enemy of enemies) {
    enemy.y += 2;
    rect(enemy.x, enemy.y, 20, 20);
    if (enemy.y > height) {
      textSize(50);
      text("Game Over!", 300, 300);
      noLoop();
    }
  }
  //collision
  for (let enemy of enemies) {
    for (let bullet of bullets) {
      if (dist(enemy.x, enemy.y, bullet.x, bullet.y) < 10) {
        enemies.splice(enemies.indexOf(enemy), 1)
        bullets.splice(bullets.indexOf(bullet), 1)
        let newEnemy = {
          x: random(0, width),
          y: random(-800, 0)
        }
        enemies.push(newEnemy);
        score += 1;
      }
    }
  }
  text(score, 25, 25);
}
//bullets when you click the mouse
function mousePressed() {
  //bullets spawning when mouse is pressed
  let bullet = {
    x: mouseX,
    y: height - 50
  }
  bullets.push(bullet);
}


