Link to program: https://salomonsimonsen8.gitlab.io/aesthetic-programming/MiniX6/index.html
<br>
Link to source: https://gitlab.com/salomonsimonsen8/aesthetic-programming/-/blob/main/MiniX5/sketch.js

![Screenshot__72_](/uploads/af0df81ad71d8d03ea2d7f19b19e6004/Screenshot__72_.png)

# Introduction & reflection

There is a "boat" and "Icebergs". Before the Icebergs go all the way down to below the screen destroy them. The Boat use a weapon to destroy them icebergs. You use your mouse. The Icebergs are set to random spawn at the top of the screen. If you press the mouse you make a bullet spawn from your boat. 
 <vr>
There is three objects, the boat that moves from side to side. There is the bullets, that moves upwards to destroy the icebergs. The icebergs as said spawn randomly from top to bottom. 
 
Originally I had a whole boat as a drawing, but I could not make it move alongside the mouse. So I ended up doing an ellipse and for the icebergs I ended up making them rectangles. I had planned to make them more detailed but as I said I had trouble making it work. So it ended up being a lot of dots with the bullets, and rectangles as "icebergs"
 
I originally wanted to make it more personal for me. To make a boat that represents my childhood trips across Greenland with Icebergs to avoid.
<br>

`In any case, and pace Kay, a computational object is a partial object: the properties and methods that define it are a necessarily selective and creative abstraction of particular capacities from the thing it models, and which specify the ways that it can be interacted with. The objects – text boxes, lists, hyperlinks and so on – that populate a webpage, for example, define more or less exactly what a user can do with them. (Fuller & Goffey, 2017)`
<br>
My game have methods that are highly like the game Galaga makes it more predictable for some users to know what to do in the game. The way it is built, the objects and the model of the icebergs going down to the ellipse that moves from side to side specifies the way the boat and the bullet can be interacted with. Also the fact that I could not make the detailed boat and icebergs makes it more likely that users can interpret it better than my intentions with the game that is based on a part of the world not many have been in. 
<br>
`Object abstraction in computing is about representation. Certain attributes and relations are abstracted from the real world, whilst simultaneously leaving details and contexts out. Let’s imagine a person as an object (rather than a subject) and consider which properties and behaviors that person might have.(Soon & Cox, 2020)`
<br>
With this game, I have hoped to create a game that somehow I could relate to. There aren’t that many games that involves my people or my culture. The boat behaves in a way to avoid the icebergs as if you collide with them it can be fatal. The Icebergs also behaves in a unpredictable way, like if you’re out sailing you don’t really know how many icebergs you’ll encounter and where you will encounter them. But the essense of this idea I will move on to the next minix hoping I can make it more detailed and more of what I originally intended. 

## syntax
In this minix I had real trouble making a class but I did manage it at the end. 

```
class Boat {
constructor () {
this.posx = mouseX;
this.posy = height-50;
}
show (){
ellipse(mouseX,this.posy, 30,30);
}
}
```
<br>

Here I have a `class` called Boat. I gave it a `constructor` that tells it it’s properties. That is where the x and y are in the screen. I have an appearance where I declare where the ellipse is by giving it the properties in the constructor. For some reason though `this.posy` didn’t work so I had to replace it with `mouseX`

One of the hardest that I had to do in this minix was to make bullets and how to try to make them go towards the icebergs. 

This is how I made the bullets



```
function mousePressed() {
//bullets spawning when mouse is pressed
let bullet = {
x: mouseX,
y: height - 50
}
bullets.push(bullet);
```
Basically I made a function where you spawn a bullet when mouse is pressed. I did that with making a variable that said you press it you make a bullet. This bullet is in the same position as the boat so I used the mouse x and height-50 as the boat only moves from side to side. 

```
for (let bullet of bullets) {
bullet.y -= 15;
circle(bullet.x, bullet.y, 10);
} 
```
Here i declare the shape and size of the bullets in a for-loop that ensures that the bullets move upwards when mouse is pressed.
