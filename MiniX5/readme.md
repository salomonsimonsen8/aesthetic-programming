Link to program: https://salomonsimonsen8.gitlab.io/aesthetic-programming/MiniX5/index.html
<br>
Link to source: https://gitlab.com/salomonsimonsen8/aesthetic-programming/-/blob/main/MiniX5/sketch.js
![Screenshot__60_](/uploads/545ce1ab2c5c72a06df5a97f85413d88/Screenshot__60_.png)

# Introduction
What I have made in my miniX is inspired by the 10 print code.
But instead of lines I made mine with rectangles and ellipses. 
Also to encourage colorfulness I made the rectangles and ellipses be in random colors. I also made a white outline.
 
## Syntaxes
My minix has 2 rules
 
 
```
if (random(1) < 0.5) {
fill(random(255), 0, random(255));
rect(x,y,spacing,spacing,);
} else {
fill(random(255),0,random(255));
ellipse(x+23,y+23,spacing,spacing);
}
```
I made an `if-statement` in my code that it choses between 0 and 1. 
If my code is less than 0.5 it makes a rectangle if it's bigger
Than 0.5 it makes an ellipse.
In the same syntax I made it chose between a random color to put in the rectangle or ellipse. 
 
```
x = x + spacing;
if(x > width) {
x = 0;
y = y + spacing;
}
```
 
Here I made an `if-statement` in code that if both rectangle and ellipses reaches the limit of the width or it's greater than width, x start over, but in the next line. To make it that way, I put ´y = y + spacing;´ to make it start over 1 line under.
 
To make the outlines I made 4 `for loops` this one is an example. 
```
for (var z = 0; z <= width; z = z + 50) {
fill(255);
stroke(1);
rect(z,-35,50,50);
} 
```
What this does is allow me to repeat rectangles without individually make them. 
We start the rectangles at 1 and stop when z is equal to width. Afterwards I made z go up after 50, this means that the rectangles are 50 wide. 
 
 
### Reflection
I didn't know what to make this minix, but I liked the idea of the 10 print idea. The little randomness and the swiping effect.
 
In my minix there is some randomness in case of if its an rectangle or an ellipse. This randomness is set by a rule. In the same rule I applied a random color in it. Making this a generative art in a way where I am using a system to make it artful. 

“Generative art refers to any art practice where [sic] artists use a system, such as a set of natural languages, rules, a computer program, a machine, or other procedural invention, which is set into motion with some degree of autonomy contributing to or resulting in a completed work of art.” (Soon & Cox, 2020)
 
My minixs randomness will never be truly random because I made it with only two options. All that my minix is following are the two rules that I made in the code. To go from start to end of the canvas and make rectangles and ellipses with different colors. When you have done that, then start from the next line and do this until the canvas is covered. 
<br>
The randomness is based of probabilities that are enforced by the rule. This randomness then becomes controlled because the rules controls the randomness. But this randomness is out of my control, so this theme of randomness is something I find interesting. I find it interesting because even though I placed the rule I cannot decide which it will become, this decision is on the code. Nevertheless, by looking at my minix’s behavior from the start it’s very predictable. It creates a line of rectangles and ellipses until it reaches the other side and then it starts again in the next line. This can raise the question of if it’s random. Yes, the rectangles and the ellipses are random and the colors are random, but at the end, the results are very similar. The only changes are yes you guessed it the rectangles and ellipses and their colors. If I were to make it more random I could have coded it where they appear in the screen randomly, but I wanted to make it smooth. So it looks like an art piece made with ellipses and rectangles and red and blue colors. 
